import express from "express";
import cors from "cors";
import morgan from "morgan";

//Importación de routes
import articuloRoutes from "./routes/articulo.js";
import periodistaRoutes from "./routes/periodista.js";

const app = express();

const PORT = process.env.PORT || 3000;

app.use(express.json());
app.use(cors({ origin: "http://localhost:5173" }));
app.use(morgan("dev"));

//Routes
app.use(articuloRoutes);
app.use(periodistaRoutes);

app.listen(PORT, () => {
  console.log(`Servidor en el puerto ${PORT}`);
});
