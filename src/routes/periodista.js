import express from "express";
import {
  getPeriodistaById,
  loginPeriodista,
  registerPeriodista,
} from "../controllers/periodista.js";

const router = express.Router();

router.get("/periodista/:id", getPeriodistaById);

router.post("/register", registerPeriodista);

router.post("/login", loginPeriodista);

export default router;
