import express from "express";
import {
  getAllArticulos,
  getArticuloById,
  addArticulo,
  updateArticulo,
  deleteArticulo,
} from "../controllers/articulo.js";
import { verifyToken } from "../controllers/auth.js";

const router = express.Router();

router.get("/articulo/", getAllArticulos);

router.get("/articulo/:id", getArticuloById);

router.post("/articulo", verifyToken, addArticulo);

router.put("/articulo/:id", verifyToken, updateArticulo);

router.delete("/articulo/:id", verifyToken, deleteArticulo);

export default router;
