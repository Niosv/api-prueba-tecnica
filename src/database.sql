--Crear base de datos
CREATE DATABASE PruebaTecnicaMegaMedia;

--Crear la tabla usuario
CREATE TABLE periodista (
	id SERIAL PRIMARY KEY,
	nombre VARCHAR(50) NOT NULL,
	apellido VARCHAR(50) NOT NULL,
	correo VARCHAR(255) NOT NULL UNIQUE,
	passwd VARCHAR(255) NOT NULL
);

-- Crear la tabla articulo
CREATE TABLE articulo (
    id SERIAL PRIMARY KEY,
    titulo VARCHAR(255) NOT NULL,
    descripcion TEXT NOT NULL,
    imagen VARCHAR(255) NOT NULL,
    url_video VARCHAR(255) NOT NULL,
	periodista INT NOT NULL,
	CONSTRAINT fk_periodista FOREIGN KEY (periodista) REFERENCES periodista(id)
);

--Insertar registros en la tabla 'periodista'
INSERT INTO periodista (nombre, apellido, correo, passwd) 
VALUES 
	('Ejemplo', 'Ejemplo', 'ejemplo@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92');
	--La contraseña es '123456' encriptada en SHA256

-- Insertar registros en la tabla 'articulo'
INSERT INTO articulo (titulo, descripcion, imagen, url_video, periodista) 
VALUES 
    ('¿Será el fin de las contraseñas?', 'Google lanzó una nueva tecnología llamada "Passkeys", con el fin de proteger nuestras cuentas y dejar en el pasado las clásicas contraseñas. En otros temas, el Ministerio de Ciencia presentó la nueva Política Nacional de Inteligencia Artificial, que busca un desarrollo de la IA y poner a Chile en la vanguardia sobre ética y responsabilidad de esta tecnología.', 'https://www.google.com/account/about/static/passkey-illustration.svg', 'HZaJ1dDBSbQ', 1),
    ('Los detalles de la Ley Karin sobre el acoso y violencia laboral en Chile', 'Francisca Jünemann, Presidenta de la Fundación ChileMujeres, explicó los nuevos avances en la Ley Karin, que perfecciona la legislación frente al acoso laboral. Alrededor de un 11% de las mujeres dice haber sufrido estos actos.', 'https://s3.amazonaws.com/gobcl-prod/filer_public_thumbnails/public_files/Campa%C3%B1as/Corona-Virus/Reportes/Nuevos-reportes/ley-karin.png__1200x630_crop_subsampling-2.png', 'b32aqMIalvM', 1),
    ('Mega y Bizarro se adjudican transmisión del Festival de Viña', 'La Municipalidad de Viña de Mar, notificó a Mega la adjudicación de derechos del Festival de Viña del Mar en una alianza con la productora Bizarro, para la transmisión del evento, desde este 2025 hasta el año 2028.', 'https://s3-mspro.nyc3.cdn.digitaloceanspaces.com/tenant/6511ab48ed370780759ae0ec/mediaLibrary/photo/c179908e-1f58-4919-9781-e594c85e0d30-medium-standard-q100.jpeg?v=1712353566', 'loyiXUbrjMI', 1);
