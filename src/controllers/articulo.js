import pool from "../database.js";
import multer from "multer";
import path from "path";
import fs from "fs";

//Configuración de multer
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./src/uploads/");
  },
  filename: function (req, file, cb) {
    const filename = `${file.originalname}-${Date.now()}${path.extname(
      file.originalname
    )}`;
    cb(null, filename);
  },
});

const upload = multer({ storage });

export const getAllArticulos = async (req, res) => {
  try {
    const query = "SELECT * FROM articulo";
    const result = await pool.query(query);
    const data = result.rows;

    //Convert imagen a base64
    const articulos = data.map((articulo) => {
      if (articulo.imagen && fs.existsSync(articulo.imagen)) {
        const imageData = fs.readFileSync(articulo.imagen);
        const base64Img = Buffer.from(imageData).toString("base64");
        return { ...articulo, base64: base64Img };
      }
      return articulo;
    });
    res.status(200).json(articulos);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const getArticuloById = async (req, res) => {
  try {
    const { id } = req.params;
    const query = "SELECT * FROM articulo WHERE id = $1";
    const result = await pool.query(query, [id]);
    if (result.rowCount === 0) {
      return res
        .status(404)
        .json({ message: "No existe este articulo", ok: false });
    }
    const data = result.rows[0];
    //Convert imagen a base64
    if (fs.existsSync(data.imagen)) {
      fs.readFile(data.imagen, (err, imageData) => {
        if (err) {
          console.error("Error al leer el archivo:", err);
          return res.status(500).json({ message: "Error al leer el archivo" });
        }
        const base64Img = Buffer.from(imageData).toString("base64");
        const articulo = { ...data, base64: base64Img };
        res.status(200).json(articulo);
      });
    } else {
      res.status(200).json(data);
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const addArticulo = async (req, res) => {
  try {
    //Upload imagen
    upload.single("imagen")(req, res, async function (err) {
      if (err) {
        return res.status(500).json({ message: err.message });
      }

      const { titulo, descripcion, url_video } = req.body;
      const periodista = req.user.id;
      const imagen = req.file.path; //Ruta de la imagen

      //Almacenar ID del video de Youtube
      const regExp =
        /^(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/(?:[^\/\n\s]+\/\S+\/|(?:v|e(?:mbed)?)\/|\S*?[?&]v=)|youtu\.be\/)([a-zA-Z0-9_-]{11})$/;
      const match = url_video.match(regExp);
      console.log(match);
      const videoId = match ? match[1] : url_video;

      const query =
        "INSERT INTO articulo (titulo, descripcion, imagen, url_video, periodista) VALUES ($1, $2, $3, $4, $5)";
      const result = await pool.query(query, [
        titulo,
        descripcion,
        imagen,
        videoId,
        periodista,
      ]);
      res
        .status(201)
        .json({ message: `Se agregó el articulo ${titulo}`, ok: true });
    });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const updateArticulo = async (req, res) => {
  try {
    // Verificar si el artículo pertenece al periodista logeado
    const queryPeriodista = "SELECT periodista FROM articulo WHERE id = $1";
    const resultPeriodista = await pool.query(queryPeriodista, [req.params.id]);
    const periodistaArticulo = resultPeriodista.rows[0].periodista;

    if (periodistaArticulo !== req.user.id) {
      return res.status(403).json({
        message: "No tienes permiso para actualizar este artículo",
        ok: false,
      });
    }

    // Upload de la nueva imagen
    upload.single("imagen")(req, res, async function (err) {
      if (err) {
        return res.status(500).json({ message: err.message });
      }

      console.log(req.body);
      console.log(req.file);
      const { titulo, descripcion, url_video } = req.body;
      const { id } = req.params;
      // Ruta de la nueva imagen, o null si no se proporcionó una nueva imagen
      let imagen = req.file ? req.file.path : null;

      if (imagen !== null) {
        //Actualiza todos los campos
        const query =
          "UPDATE articulo SET titulo = $1, descripcion = $2, imagen = $3, url_video = $4 WHERE id = $5";
        const result = await pool.query(query, [
          titulo,
          descripcion,
          imagen,
          url_video,
          id,
        ]);
        if (result.rowCount === 0) {
          return res
            .status(404)
            .json({ message: "No se encontró el artículo", ok: false });
        }
      } else {
        //No actualiza la imagen
        const query =
          "UPDATE articulo SET titulo = $1, descripcion = $2, url_video = $3 WHERE id = $4";
        const result = await pool.query(query, [
          titulo,
          descripcion,
          url_video,
          id,
        ]);
        if (result.rowCount === 0) {
          console.log("FINAL", console.log(result));
          return res
            .status(404)
            .json({ message: "No se encontró el artículo", ok: false });
        }
      }

      res.status(200).json({ message: "Se actualizó el artículo", ok: true });
    });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const deleteArticulo = async (req, res) => {
  try {
    // Verificar si el artículo pertenece al periodista logeado
    const queryPeriodista = "SELECT periodista FROM articulo WHERE id = $1";
    const resultPeriodista = await pool.query(queryPeriodista, [req.params.id]);
    const periodistaArticulo = resultPeriodista.rows[0].periodista;

    if (periodistaArticulo !== req.user.id) {
      return res.status(403).json({
        message: "No tienes permiso para eliminar este artículo",
        ok: false,
      });
    }

    const { id } = req.params;
    const query = "DELETE FROM articulo WHERE id = $1";
    const result = await pool.query(query, [id]);
    if (result.rowCount === 0) {
      return res
        .status(404)
        .json({ message: "No se encontró el articulo", ok: false });
    }
    res.status(200).json({ message: "Se eliminó el articulo", ok: true });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
