import pool from "../database.js";
import { generateToken } from "./auth.js";
import crypto from "crypto";

export const getPeriodistaById = async (req, res) => {
  try {
    const { id } = req.params;
    const query = "SELECT * FROM periodista WHERE id = $1";
    const result = await pool.query(query, [id]);
    if (result.rowCount === 0) {
      return res
        .status(404)
        .json({ message: "No se encontró el periodista", ok: false });
    }
    res.status(200).json(result.rows[0]);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const registerPeriodista = async (req, res) => {
  try {
    const { nombre, apellido, correo, passwd } = req.body;
    const query =
      "INSERT INTO periodista (nombre, apellido, correo, passwd) VALUES ($1, $2, $3, $4) ON CONFLICT (correo) DO NOTHING returning id, correo";
    const hash = crypto.createHash("sha256");
    hash.update(passwd);
    const passwordHashed = hash.digest("hex");

    const result = await pool.query(query, [
      nombre,
      apellido,
      correo,
      passwordHashed,
    ]);
    if (result.rowCount === 0) {
      return res
        .status(400)
        .json({ message: "El correo ya está en uso", ok: false });
    }
    const user = result.rows[0];
    const payload = { id: user.id, correo: user.correo };
    const token = generateToken(payload);
    res.status(200).json({
      message: `Se registró al periodista ${nombre} ${apellido}`,
      ok: true,
      token,
      user,
    });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const loginPeriodista = async (req, res) => {
  try {
    const { correo, passwd } = req.body;
    const query = "SELECT * FROM periodista WHERE correo = $1";
    const result = await pool.query(query, [correo]);
    if (result.rowCount === 0) {
      return res.status(404).json({
        message: "No existe un periodista con este correo",
        ok: false,
      });
    }
    const user = result.rows[0];

    const hash = crypto.createHash("sha256");
    hash.update(passwd);
    const passwordHashed = hash.digest("hex");

    if (user.passwd === passwordHashed) {
      const payload = {
        id: user.id,
        correo: user.correo,
      };
      const token = generateToken(payload);
      return res
        .status(200)
        .json({ message: "Login éxitoso", ok: true, token, user: payload });
    }
    res.status(401).json({ message: "Contraseña inválida", ok: false });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
