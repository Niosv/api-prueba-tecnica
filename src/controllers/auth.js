import jwt from "jsonwebtoken";

const secretKey = "PruebaTecnica";

export const generateToken = (payload) => {
  return jwt.sign(payload, secretKey, { expiresIn: "1h" });
};

export const verifyToken = (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (!authHeader) {
    return res
      .status(401)
      .json({ message: "Token de acceso no proporcionado", ok: false });
  }

  if (!authHeader || !authHeader.startsWith("Bearer ")) {
    return res.status(401).json({ error: "Acceso no autorizado" });
  }

  const token = authHeader.split(" ")[1]; //Excluye 'Bearer '

  try {
    const decoded = jwt.verify(token, secretKey);
    req.user = decoded;
    next();
  } catch (error) {
    console.error("Error al verificar el token", error);
    return res
      .status(401)
      .json({ message: "Token de acceso inválido", ok: false });
  }
};
