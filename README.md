### API para Prueba Técnica

API de prueba técnica para MegaMedia desarrollada en NodeJS con ExpressJS.

### Estructura de carpetas

## SRC

Esta carpeta contiene todo el código de la API. Contiene las carpetas **Routes**, **Controllers** y **Uploads**, además también de los archivos:

- _index.js_ => Configuración de la API.
- _database.js_ => Conexión con la base de datos PostgreSQL.
  > Puede ser necesario tener que configurar el puerto, usuario y contraseña de la base de datos.
- _database.sql_ => SQL de creación e inserción de base de datos PostgreSQL.

# Routes

Declara las rutas de los diferentes endpoints de la API. Contiene dos archivos, referentes al módulo de articulos y el de periodistas.

# Controllers

Declara las funciones que gatillan los endpoints de la API. Contiene tres archivos, referentes a los módulos de articulos, periodistas y autenticación(Auth).

# Uploads

Almacena las imagenes relacionadas a los articulos cuando estos son agregados y/o modificados.
